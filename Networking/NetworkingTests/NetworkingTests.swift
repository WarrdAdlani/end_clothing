//
//  NetworkingTests.swift
//  NetworkingTests
//
//  Created by Warrd Adlani on 30/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import XCTest
@testable import Networking

class NetworkingTests: XCTestCase {
    var stubResponse = "[{\"id\": \"1\",\"name\": \"Test Shirt\", \"price\": \"£199\", \"image\": \"fake.image.url.com\"}]"
    
    func testRequestSuccess() {
        let request = GetProducts()
        let mockDispatcher = MockDispatcher(jsonStub: stubResponse)
        request.execute(dispatcher: mockDispatcher, onSuccess: { (products: [StubProduct]) in
            let product = products.first
            XCTAssertEqual(product?.id, "1")
            XCTAssertEqual(product?.name, "Test Shirt")
            XCTAssertEqual(product?.image, "fake.image.url.com")
        }) { (error : Error) in
            XCTFail()
        }
    }
    
    func testRequestFailure() {
        let request = GetProducts()
        let mockDispatcher = MockDispatcher(jsonStub: nil)
        request.execute(dispatcher: mockDispatcher, onSuccess: { (products: [StubProduct]) in
            XCTFail()
        }) { (error : Error) in
            let error = error as NSError
            
            XCTAssert(error.code == 101)
            XCTAssert(error.domain == "network failure")
        }
    }
    
    func testWebServiceHasDispatcherSet() {
        let mockDispatcher = MockDispatcher(jsonStub: nil)
        let webService = WebService(dispatcher: mockDispatcher)
        XCTAssertTrue(webService.dispatcher as! MockDispatcher == mockDispatcher)
    }
    
    func testWebServiceSetsDefaultDispatcher() {
        let webService = WebService()
        let dispatcher = webService.dispatcher
        XCTAssertNotNil(dispatcher)
    }
    
    func testWebServiceRequestSuccess() {
        let mockDispatcher = MockDispatcher(jsonStub: stubResponse)
        let mockWebService = MockWebService(dispatcher: mockDispatcher)
        mockWebService.getProducts { (products, error) in
            if let products = products {
                let product = products.first
                XCTAssertEqual(product?.id, "1")
                XCTAssertEqual(product?.name, "Test Shirt")
                XCTAssertEqual(product?.image, "fake.image.url.com")
            } else {
                XCTFail()
            }
        }
    }
    
    func testWebServiceRequestFailure() {
        let mockDispatcher = MockDispatcher(jsonStub: nil)
        let mockWebService = MockWebService(dispatcher: mockDispatcher)
        mockWebService.getProducts { (products, error) in
            if let _ = products {
                XCTFail()
            } else {
                guard let error = error as NSError? else { fatalError() }
                
                XCTAssert(error.code == 101)
                XCTAssert(error.domain == "network failure")
            }
        }
    }
    
    func testDispatcherReturnsParsesJSONAndReturnsModel() {
        let mockDispatcher = MockDispatcher(jsonStub: stubResponse)
        mockDispatcher.dispatch(request: Request(path: nil), onSuccess: { (data) in
            let products = try? JSONDecoder().decode(GetProducts.ResponseType.self, from: data)
            XCTAssertNotNil(products)
            let product = products?.first
            XCTAssertEqual(product?.id, "1")
            XCTAssertEqual(product?.name, "Test Shirt")
            XCTAssertEqual(product?.image, "fake.image.url.com")
        }) { (error) in
            XCTFail()
        }
    }
}

struct MockDispatcher: NetworkDispatcherProtocol, Equatable {
    let jsonStub: String?
    
    func dispatch(request: RequestProtocol, onSuccess: @escaping (Data) -> Void, onError: @escaping (Error) -> Void) {
        if let jsonData = jsonStub?.data(using: .utf8, allowLossyConversion: false) {
            onSuccess(jsonData)
        } else {
            onError(NSError.init(domain: "network failure", code: 101, userInfo: nil))
        }
    }
}

struct StubProduct: Codable {
    let id: String
    let name: String
    let image: String
}

struct StubProducts: Codable {
    let products: [StubProduct]
}

// MARK: Requests
struct GetProducts: RequestTypeProtocol {
    typealias ResponseType = [StubProduct]
    var data: Request {
        return Request(path: nil)
    }
}

struct MockWebService: WebServiceProtocol {
    var dispatcher: NetworkDispatcherProtocol!
}

extension MockWebService {
    func getProducts(completion: @escaping ([StubProduct]?, Error?)->()) {
        let requestProducts = GetProducts()
        requestProducts.execute(dispatcher: dispatcher, onSuccess: { (products: [StubProduct]) in
            completion(products, nil)
        }) { (error: Error) in
            completion(nil, error)
        }
    }
}

