//
//  EndPoints.swift
//  Networking
//
//  Created by Warrd Adlani on 30/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation

enum EndPoints: String {
    case products = "https://www.endclothing.com/media/catalog/example.json"
}
