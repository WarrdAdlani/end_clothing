//
//  Product.swift
//  Domain
//
//  Created by Warrd Adlani on 30/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation

public protocol ProductProtocol {
    var id: String { get }
    var name: String { get }
    var price: String { get }
    var image: String { get }
}

// MARK: - Product
public struct Product: ProductProtocol, Codable {
    public let id: String
    public let name: String
    public let price: String
    public let image: String
}
