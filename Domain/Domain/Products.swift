//
//  Products.swift
//  Domain
//
//  Created by Warrd Adlani on 30/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation

// MARK: - Products

public protocol ProductsProtocol {}

public struct Products: ProductsProtocol, Codable {
    public let products: [Product]
    public let title: String
    public let productCount: Int
    
    enum CodingKeys: String, CodingKey {
        case products, title
        case productCount = "product_count"
    }
}
