END Clothing

I've built the app using MVVM+C architecture as this is similar to that which is being used on the END clothing app on the appstore.

I've taken the liberty of building some of the UI based on assumptions and concepts where applicable. An example of this is the bottom tool bar that has only one icon which is just a place holder.

I added another screen, that very badly shows the product image :S, just to illustrate using coordinators to manage view screen flows