//
//  END_ClothingUITests.swift
//  END_ClothingUITests
//
//  Created by Warrd Adlani on 02/08/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import XCTest

class END_ClothingUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func test_Tapping_Sort_Shows_Filters() {
        let app = XCUIApplication()
        app.buttons["Sort"].tap()
        
        app.otherElements.containing(.navigationBar, identifier:"NEW THIS WEEK").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .button).element.tap()
    }
    
    func test_Tapping_View_Shows_Filters() {
        let app = XCUIApplication()
        app.buttons["View"].tap()
        
        app.otherElements.containing(.navigationBar, identifier:"NEW THIS WEEK").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .button).element.tap()
    }
    
    func test_Tapping_Filter_Shows_Filters() {
        let app = XCUIApplication()
        app.buttons["Filter"].tap()
        
        app.otherElements.containing(.navigationBar, identifier:"NEW THIS WEEK").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element(boundBy: 1).children(matching: .button).element.tap()
    }
    
    func test_Tapping_Product_Opens_Product_Details() {
        XCUIApplication().collectionViews.children(matching: .cell).element(boundBy: 0).otherElements.containing(.staticText, identifier:"Test Shirt").element.tap()   
    }
}
