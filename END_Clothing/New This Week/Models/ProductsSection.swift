//
//  ProductsSection.swift
//  END_Clothing
//
//  Created by Warrd Adlani on 01/08/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import Domain
import RxDataSources

struct ProductsSection {
    var header: String {
        willSet {
            
        }
        didSet {
            
        }
    }
    var items: [Product]
}

extension ProductsSection : SectionModelType {
    typealias Item = Product
    
    var identity: String {
        return header
    }
    
    init(original: ProductsSection, items: [Item]) {
        self = original
        self.items = items
    }
}
