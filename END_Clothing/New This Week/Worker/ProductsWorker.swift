//
//  ProductsWorker.swift
//  END_Clothing
//
//  Created by Warrd Adlani on 30/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import Domain
import Networking

struct ProductsWorker {
    fileprivate var webService: WebService
    
    init(webService: WebService) {
        self.webService = webService
    }
    
    func getProducts(completion: @escaping NetworkResponseCompletion) {
        webService.getProducts(completion: completion)
    }
}
