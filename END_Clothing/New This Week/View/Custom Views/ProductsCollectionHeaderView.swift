//
//  ProductsCollectionHeaderView.swift
//  END_Clothing
//
//  Created by Warrd Adlani on 01/08/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

class ProductsCollectionHeaderView: UICollectionReusableView {
    var titleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        titleLabel = UILabel(frame: frame)
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.systemFont(ofSize: 14, weight: .light)
        self.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

