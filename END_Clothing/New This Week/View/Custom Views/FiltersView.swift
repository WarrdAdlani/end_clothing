//
//  FiltersView.swift
//  END_Clothing
//
//  Created by Warrd Adlani on 02/08/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

class FiltersView: UIView {
    public let height: CGFloat = 256.0
    public let hiddenValue: CGFloat = 256.0
    fileprivate var closeButton: UIButton
    fileprivate let label: UILabel
    
    override init(frame: CGRect) {
        closeButton = UIButton(frame: CGRect.zero)
        label = UILabel(frame: CGRect.zero)
        
        super.init(frame: frame)
        
        closeButton.addTarget(self, action: #selector(close), for: .touchUpInside)
        self.addSubview(closeButton)
        closeButton.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        self.backgroundColor = .white
        
        label.text = "Sampel filters view"
        label.textColor = .black
        
        self.addSubview(label)
        
        label.snp.makeConstraints({ (make) in
            make.center.equalToSuperview()
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func showFilters(_ show: Bool, title: String?) {
        if let title = title {
            label.text = title
        }
        if show {
            self.snp.updateConstraints { (make) in
                make.bottom.equalTo(0)
            }
        } else {
            self.snp.updateConstraints { (make) in
                make.bottom.equalTo(hiddenValue)
                UIView.animate(withDuration: 0.3) { [weak superview] in
                    superview?.layoutIfNeeded()
                }
            }
        }
    }
    
    @objc func close(sender: UIButton) {
        self.showFilters(false, title: nil)
    }
}
