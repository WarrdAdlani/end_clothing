//
//  ProductsCollectionCell.swift
//  END_Clothing
//
//  Created by Warrd Adlani on 30/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit
import SnapKit
import SDWebImage

protocol ProductCellPresentable {
    func setupCell(with viewModel: ProductsCellViewModel)
}

class ProductsCollectionCell: UICollectionViewCell {
    fileprivate var viewModel: ProductsCellViewModel!
    fileprivate var productImageView: UIImageView!
    fileprivate var productNameLabel: ProductLabel!
    fileprivate var productPriceLabel: ProductLabel!
    fileprivate var stackView: UIStackView!
    
    override init(frame: CGRect) {
        super.init(frame: CGRect.zero)
        
        stackView = UIStackView(frame: CGRect.zero)
        stackView.distribution = .fillProportionally
        stackView.alignment = .fill
        stackView.axis = .vertical
        stackView.spacing = 4
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        productImageView = UIImageView(frame: CGRect.zero)
        productImageView.contentMode = .scaleAspectFit
        productImageView.clipsToBounds = true
        
        productNameLabel = ProductLabel(frame: CGRect.zero)
        productNameLabel.setupProductsLabel(with: 2)
        
        productPriceLabel = ProductLabel(frame: CGRect.zero)
        productPriceLabel.setupProductsLabel()
        
        stackView.addArrangedSubview(productImageView)
        stackView.addArrangedSubview(productNameLabel)
        stackView.addArrangedSubview(productPriceLabel)
        
        addSubview(stackView)
        
        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ProductsCollectionCell: ProductCellPresentable {
    func setupCell(with viewModel: ProductsCellViewModel) {
        self.viewModel = viewModel
        productImageView?.sd_setImage(with: URL(string: viewModel.image), completed: nil)
        productNameLabel.text = viewModel.name
        productPriceLabel.text = viewModel.price
        
    }
}

class ProductLabel: UILabel {
    func setupProductsLabel(with numberOfLines: Int = 1) {
        textAlignment = .center
        self.numberOfLines = numberOfLines
        font = UIFont.systemFont(ofSize: 14, weight: .light)
        setContentHuggingPriority(.required, for: .horizontal)
        setContentHuggingPriority(.required, for: .vertical)
        setContentCompressionResistancePriority(.required, for: .horizontal)
        setContentCompressionResistancePriority(.required, for: .vertical)
    }
}
