//
//  ProductsCollectionViewModel.swift
//  END_Clothing
//
//  Created by Warrd Adlani on 30/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import Domain

struct ProductsCellViewModel {
    fileprivate var product: Product
    
    public var name: String {
        return product.name
    }
    public var price: String {
        return product.price
    }
    public var image: String {
        return product.image
    }
    
    init(product: Product) {
        self.product = product
    }
}
