//
//  NewThisWeekViewController.swift
//  END_Clothing
//
//  Created by Warrd Adlani on 30/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit
import SnapKit
import RxCocoa
import RxSwift
import RxDataSources
import Domain

class NewThisWeekViewController: UIViewController {
    fileprivate var currentOffset: CGFloat = 0.0
    fileprivate let toolBarHeight: CGFloat = 44.0
    fileprivate let bottomToolBarHeight: CGFloat = 64.0
    fileprivate let defaultAnimationDuration = 0.25
    
    var viewModel: NewThisWeekViewModel
    fileprivate var collectionView: UICollectionView!
    fileprivate lazy var topToolBarView: UIView = {
        let toolBarView = ToolBarView(frame: CGRect.zero)
        toolBarView.viewBlock = { [weak self] in
            self?.showView()
        }
        toolBarView.sortBlock = { [weak self] in
            self?.showSort()
        }
        toolBarView.filterBlock = { [weak self] in
            self?.showFilter()
        }
        return toolBarView
    }()
    fileprivate lazy var bottomToolBarView: UIView = {
        let bottomToolBarView = UIView(frame: CGRect.zero)
        let endLogoImageView = UIImageView(image: UIImage(named: "end_logo"))
        endLogoImageView.frame = CGRect.init(x: 0, y: 0, width: 44, height: 44)
        endLogoImageView.contentMode = .scaleAspectFit
        bottomToolBarView.addSubview(endLogoImageView)
        endLogoImageView.snp.makeConstraints({ (make) in
            make.top.equalToSuperview().inset(4)
            make.centerX.equalToSuperview()
            make.width.equalTo(44)
            make.height.equalTo(44)
        })
        endLogoImageView.alpha = 0.4
        bottomToolBarView.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.2).cgColor
        bottomToolBarView.layer.borderWidth = 1.0
        bottomToolBarView.backgroundColor = .white
        return bottomToolBarView
    }()
    fileprivate lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.hidesWhenStopped = true
        return activityIndicator
    }()
    fileprivate lazy var filtersView: FiltersView = {
        let filtersView = FiltersView(frame: CGRect.zero)
        return filtersView
    }()
    fileprivate lazy var mainView: UIView = {
        let mainView = UIView(frame: UIScreen.main.bounds)
        mainView.backgroundColor = .white
        return mainView
    }()
    
    weak var coordinator: MainCoordinator?
    
    // RxSwift properties
    private let disposeBag = DisposeBag()
    
    init(viewModel: NewThisWeekViewModel, coordinator: MainCoordinator?) {
        self.viewModel = viewModel
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
    }
    
    public var isAnimating: Binder<Bool> {
        return Binder(self, binding: { [weak self] (vc, active) in
            if active {
                self?.activityIndicator.startAnimating()
            } else {
                self?.activityIndicator.stopAnimating()
            }
        })
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        // Setup main view
        view = mainView
        
        setupUI()
        bindViewModel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "NEW THIS WEEK"
        getProducts()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        topToolBarView.snp.updateConstraints { make in
            make.top.equalTo(0)
        }
    }
}

extension NewThisWeekViewController {
    func setupUI() {
        // Setup collection view
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: 36)
        
        collectionView = UICollectionView(frame: mainView.bounds, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        collectionView.register(ProductsCollectionCell.self, forCellWithReuseIdentifier: "cell")
        
        collectionView.contentInset = UIEdgeInsets(top: toolBarHeight, left: 0, bottom: 0, right: 0)
        collectionView.register(ProductsCollectionHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "header")
        mainView.addSubview(collectionView)
        
        collectionView.snp.makeConstraints { make in
            make.edges.equalTo(mainView)
        }
        
        mainView.addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
        }
        
        mainView.addSubview(topToolBarView)
        topToolBarView.snp.makeConstraints { make in
            make.top.equalTo(0)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(toolBarHeight)
        }
        
        mainView.addSubview(bottomToolBarView)
        
        bottomToolBarView.snp.makeConstraints { (make) in
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.bottom.equalTo(0)
            make.height.equalTo(bottomToolBarHeight)
        }
        
        mainView.addSubview(filtersView)
        // Demo purposes only. Purposefully incomplete
        filtersView.snp.makeConstraints({ (make) in
            make.width.equalToSuperview()
            make.height.equalTo(filtersView.height)
            make.bottom.equalTo(filtersView.hiddenValue)
        })
        
        // Make two cells per row
        let numberOfCellsPerRow: CGFloat = 2
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            let horizontalSpacing = flowLayout.scrollDirection == .vertical ? flowLayout.minimumInteritemSpacing : flowLayout.minimumLineSpacing
            let cellWidth = (mainView.frame.width - max(0, numberOfCellsPerRow - 1)*horizontalSpacing)/numberOfCellsPerRow
            flowLayout.itemSize = CGSize(width: cellWidth, height: cellWidth)
        }
    }
    
    func getProducts() {
        viewModel.getProducts()
    }
    
    @objc func refresh() {
        getProducts()
    }
    
    // MARK: Show filters methods
    @objc func showSort() {
        showFilters(with: "Sort")
    }
    
    @objc func showFilter() {
        showFilters(with: "Filter")
    }
    
    @objc func showView() {
       showFilters(with: "View")
    }
    
    func showFilters(with title: String) {
        filtersView.showFilters(true, title: title)
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.mainView.layoutIfNeeded()
        }
    }
    
    func bindViewModel() {
        // Binding the view model to activity indicator
        viewModel.loading.bind(to: self.isAnimating).disposed(by: disposeBag)
        // Subscribing to events regarding products in view model to collectionview
        viewModel.products.subscribe(onNext: { [weak self] products in
                self?.bindCollectionView(with: products)
                self?.collectionView.reloadData()
            }, onError: { [weak self] (error) in
                self?.showErrorAlert(with: error)
        }).disposed(by: disposeBag)
        
        // Animation of toolbars
        collectionView.rx.didScroll.subscribe({ [weak self] _ in
            if let scrollView = self?.collectionView,
                let viewModel = self?.viewModel,
                let topToolBarView = self?.topToolBarView,
                let bottomToolBarView = self?.bottomToolBarView,
                let mainView = self?.mainView,
                let toolBarHeight = self?.toolBarHeight,
                let currentOffset = self?.currentOffset,
                let defaultAnimationDuration = self?.defaultAnimationDuration,
                let bottomToolBarHeight = self?.bottomToolBarHeight {
                
                let scrollPos: CGFloat = scrollView.contentOffset.y
                
                // Prevents collection view animating constraints by pre-laying out constraints
                scrollView.layoutIfNeeded()
                
                if viewModel.numberOfRows > 0 {
                    if scrollPos > currentOffset {
                        topToolBarView.snp.updateConstraints { make in
                            make.top.equalTo(-toolBarHeight)
                        }
                        bottomToolBarView.snp.updateConstraints { (make) in
                            make.bottom.equalTo(bottomToolBarHeight)
                        }
                        UIView.animate(withDuration: defaultAnimationDuration) { [weak self] in
                            self?.mainView.layoutIfNeeded()
                        }
                    } else {
                        if viewModel.numberOfRows > 0 {
                            topToolBarView.snp.updateConstraints { make in
                                make.top.equalTo(0)
                            }
                            bottomToolBarView.snp.updateConstraints { (make) in
                                make.bottom.equalTo(0)
                            }
                            UIView.animate(withDuration: defaultAnimationDuration) {
                                mainView.layoutIfNeeded()
                            }
                        }
                    }
                }
            }
        }).disposed(by: disposeBag)
    }
    
    func bindCollectionView(with products: Products) {
        // Setting up datasource for header view
        let dataSource = RxCollectionViewSectionedReloadDataSource<ProductsSection>(
            configureCell: { (dataSource, collectionView, indexPath, product) in
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
                                                              for: indexPath) as! ProductsCollectionCell
                let viewModel = ProductsCellViewModel(product: product)
                cell.setupCell(with: viewModel)
                return cell
        })
        
        // Configuring header view
        dataSource.configureSupplementaryView = { (dataSource, collectionView, kind, indexPath) in
            if kind == UICollectionView.elementKindSectionHeader {
                let section = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath) as! ProductsCollectionHeaderView
                let title = "\(dataSource[0].items.count) Items"
                section.titleLabel.text = title
                return section
            } else {
                return UICollectionReusableView()
            }
        }
        
        let sections = Observable.just([ProductsSection(header: products.title, items: products.products)])
        sections.bind(to: collectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        // Item Selection
        Observable.zip(collectionView.rx.itemSelected
                ,collectionView.rx.modelSelected(Product.self)
            ).bind{ [weak self] indexPath, product in
                self?.coordinator?.showProduct(with: product)
            }.disposed(by: disposeBag)
        
        // Animation for toolbars
        collectionView.rx.willBeginDragging.subscribe({ [weak self] _ in
            if let contentOffset = self?.collectionView.contentOffset.y {
                self?.currentOffset = contentOffset
            }
        }).disposed(by: disposeBag)
    }
}



