//
//  NewThisWeekViewModel.swift
//  END_Clothing
//
//  Created by Warrd Adlani on 30/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import Networking
import Domain
import RxSwift

class NewThisWeekViewModel {
    public var loading: BehaviorSubject<Bool> = BehaviorSubject(value: false)
    var products = PublishSubject<Products>()
    var productList = PublishSubject<[Product]>()
    var numberOfRows: Int = 0
    var productsTitle: String?
    
    let webService: WebService
    var productsWorker: ProductsWorker?
    private var disposeBag = DisposeBag()
    
    init(webService: WebService) {
        self.webService = webService
        productsWorker = ProductsWorker(webService: webService)
        products.disposed(by: disposeBag)
    }
    
    func getProducts() {
        loading.onNext(true)
        productsWorker?.getProducts { [weak self] (result, error) in
            self?.loading.onNext(false)
            if let error = error {
                self?.products.onError(error)
            } else if let products = result as? Products {
                self?.productsTitle = products.title
                self?.products.onNext(products)
                self?.productList.onNext(products.products)
                self?.numberOfRows = products.products.count
            }
        }
    }
    
    deinit {
        products.dispose()
    }
}
