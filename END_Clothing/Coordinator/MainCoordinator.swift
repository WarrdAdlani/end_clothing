//
//  MainCoordinator.swift
//  END_Clothing
//
//  Created by Warrd Adlani on 30/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import UIKit
import Networking
import Domain

class MainCoordinator: Coordinator {
    var childCoordinators: [Coordinator]
    weak var navigationController: UINavigationController?
    var newThisWeekViewController: NewThisWeekViewController?
    
    init(navigationController: UINavigationController) {
        navigationController.navigationBar.tintColor = .black
        navigationController.navigationBar.backgroundColor = .white
        navigationController.navigationBar.isTranslucent = false
        
        self.navigationController = navigationController
        self.childCoordinators = [Coordinator]()
    }
    
    func start() {
        let networkDispatcher = NetworkDispatcher()
        let webService = WebService(dispatcher: networkDispatcher)
        let viewModel = NewThisWeekViewModel(webService: webService)
        let newThisWeekViewController = NewThisWeekViewController(viewModel: viewModel, coordinator: self)
        self.newThisWeekViewController = newThisWeekViewController
        navigationController?.pushViewController(newThisWeekViewController, animated: false)
    }
    
    func showProduct(with product: ProductProtocol) {
        let productDetails = ProductDetailsViewController(product: product, coordinator: self)
        navigationController?.pushViewController(productDetails, animated: false)
    }
}
