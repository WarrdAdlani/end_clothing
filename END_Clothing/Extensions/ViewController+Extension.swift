//
//  ViewController+Extension.swift
//  END_Clothing
//
//  Created by Warrd Adlani on 30/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

extension UIViewController {
    func showErrorAlert(with error: Error, title: String = "Error") {
        let alert = UIAlertController(title: title, message: error.localizedDescription, preferredStyle: .alert)
        let okay = UIAlertAction(title: "okay", style: .default, handler: nil)
        present(alert, animated: false, completion: nil)
        alert.addAction(okay)
    }
}
