//
//  Protocols.swift
//  END_Clothing
//
//  Created by Warrd Adlani on 30/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import Foundation
import UIKit

protocol Coordinator: class {
    var childCoordinators: [Coordinator] { get set}
    var navigationController: UINavigationController? { get set }
    func start()
}
