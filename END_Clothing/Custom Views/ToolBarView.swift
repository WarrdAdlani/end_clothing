//
//  ToolBarView.swift
//  END_Clothing
//
//  Created by Warrd Adlani on 30/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit

class ToolBarView: UIView {
    var sortBlock: (()->())?
    var viewBlock: (()->())?
    var filterBlock: (()->())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        let stackView = UIStackView(frame: CGRect.zero)
        
        stackView.axis = .horizontal
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        
        let sortButton = UIButton(frame: CGRect.zero)
        let viewButton = UIButton(frame: CGRect.zero)
        let filterView = UIButton(frame: CGRect.zero)
        
        sortButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .light)
        viewButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .light)
        filterView.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .light)
        
        sortButton.setTitleColor(.black, for: .normal)
        viewButton.setTitleColor(.black, for: .normal)
        filterView.setTitleColor(.black, for: .normal)
        
        sortButton.setTitle("Sort", for: .normal)
        viewButton.setTitle("View", for: .normal)
        filterView.setTitle("Filter", for: .normal)
        
        sortButton.addTarget(self, action: #selector(sort), for: .touchUpInside)
        viewButton.addTarget(self, action: #selector(view), for: .touchUpInside)
        filterView.addTarget(self, action: #selector(filter), for: .touchUpInside)
        
        sortButton.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.1).cgColor
        sortButton.layer.borderWidth = 1
        viewButton.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.1).cgColor
        viewButton.layer.borderWidth = 1
        filterView.layer.borderColor = UIColor.lightGray.withAlphaComponent(0.1).cgColor
        filterView.layer.borderWidth = 1
        
        addSubview(stackView)
        
        stackView.snp.makeConstraints{ (make) in
            make.edges.equalToSuperview()
        }
        
        stackView.addArrangedSubview(sortButton)
        stackView.addArrangedSubview(viewButton)
        stackView.addArrangedSubview(filterView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func sort() {
        sortBlock?()
    }
    
    @objc func view() {
        viewBlock?()
    }
    
    @objc func filter() {
        filterBlock?()
    }
}
