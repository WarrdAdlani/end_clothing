//
//  ProductDetailsViewController.swift
//  END_Clothing
//
//  Created by Warrd Adlani on 02/08/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import UIKit
import Domain
import SDWebImage

class ProductDetailsViewController: UIViewController {
    
    fileprivate lazy var mainView: UIView = {
        let mainView = UIView(frame: UIScreen.main.bounds)
        mainView.backgroundColor = .white
        return mainView
    }()
    
    fileprivate var imageView: UIImageView? = nil
    weak var coordinator: MainCoordinator?
    
    var product: ProductProtocol
    
    init(product: ProductProtocol, coordinator: MainCoordinator) {
        self.product = product
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        // Setup main view
        view = mainView
        setupUI()
    }
    
    func setupUI() {
        imageView = UIImageView(frame: CGRect.zero)
        imageView?.contentMode = .scaleAspectFit
        mainView.addSubview(imageView!)
        imageView!.snp.makeConstraints { (make) in
            make.topMargin.equalToSuperview().inset(16)
            make.leftMargin.equalToSuperview().inset(16)
            make.rightMargin.equalToSuperview().inset(16)
            make.bottomMargin.lessThanOrEqualToSuperview().inset(16)
            make.height.equalTo(256).priority(.required)
        }
        
        imageView!.sd_setImage(with: URL(string: product.image), completed: nil)
    }
}
