//
//  NewThisWeekViewModelTests.swift
//  END_ClothingTests
//
//  Created by Warrd Adlani on 02/08/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import XCTest
import Domain
import Networking

@testable import END_Clothing

class NewThisWeekViewModelTests: XCTestCase {
    var navC: UINavigationController!
    var coordinator: MockMainCoordinator!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        navC = UINavigationController()
        coordinator = MockMainCoordinator(navigationController: navC)
    }

    override func tearDown() {
        navC = nil
        coordinator = nil
    }
    
    func test_ViewModel_Is_Initialized() {
        coordinator.start()
        let vc: NewThisWeekViewController = coordinator.navigationController?.viewControllers.first as! NewThisWeekViewController
        
        XCTAssertNotNil(vc.viewModel)
    }
    
    func test_ViewModel_Correctly_Initializes_Dependancies() {
        coordinator.start()
        let vc: NewThisWeekViewController = coordinator.navigationController?.viewControllers.first as! NewThisWeekViewController
        
        XCTAssertNotNil(vc.viewModel.webService)
        XCTAssertNotNil(vc.viewModel.productsWorker)
    }
}
