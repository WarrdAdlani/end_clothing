//
//  END_ClothingTests.swift
//  END_ClothingTests
//
//  Created by Warrd Adlani on 30/07/2019.
//  Copyright © 2019 Warrd Adlani. All rights reserved.
//

import XCTest
import Domain

@testable import END_Clothing

class CoordinatorTests: XCTestCase {
    var navController: UINavigationController!
    var coordinator: MockMainCoordinator!
    
    override func setUp() {
        navController = UINavigationController()
        coordinator = MockMainCoordinator(navigationController: navController)
    }
    override func tearDown() {
        navController = nil
        coordinator = nil
    }
    
    func test_MainCoordinator_Has_NavigationController() {
        XCTAssertNotNil(coordinator.navigationController)
    }
    
    func test_MainCoordinator_Creates_Stack() {
        coordinator.start()
        XCTAssertNotNil(coordinator.navigationController)
        XCTAssertNotNil(coordinator.newThisWeekViewController)
    }
    
    func test_MainCoordinator_Start() {
        coordinator.start()
        XCTAssertTrue(coordinator.spyDidCallStartMethod)
    }
    
    func test_MainCoordinator_Pushes_Product_Details() {
        coordinator.start()
        let productStub = ProductStub()
        coordinator.showProduct(with: productStub)
        XCTAssertTrue(coordinator.spyDidPushDetailsViewController)
        XCTAssertTrue(coordinator.navigationController?.viewControllers.count == 2)
        XCTAssertTrue(coordinator.navigationController?.viewControllers.last is ProductDetailsViewController)
    }
}

class MockMainCoordinator: MainCoordinator {
    var spyDidPushDetailsViewController = false
    var spyDidCallStartMethod = false
    
    override func start() {
        super.start()
        spyDidCallStartMethod = true
    }
    
    override func showProduct(with product: ProductProtocol) {
        spyDidPushDetailsViewController = true
        super.showProduct(with: product)
    }
}

class ProductStub: ProductProtocol {
    var id: String = "1"
    var name: String = "sneakers"
    var price: String = "199"
    var image: String = "fake.url.com"
}
